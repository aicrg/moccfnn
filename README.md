This algorithm is designed and tested to be run using a G++ compiler. You will also need to create your own training and testing data sets. For more details check the wiki. 

     Algorithm
	-----------
	1. Initialize the sub-populations
	2. Evaluate the sub-populations on the two given objectives using random collaborations
	3. Rank the sub-populations and identify non-dominated individuals
	4. Assign Pareto front
	While Max_Evaluations not Reached
		For all sub-pops	
			5. Select parents and generate offspring
			6. update the sub-populations
			7. Rank Sub-populations & Identify non-dominated individuals
			8. Assign Pareto Front	
		End For
	End While