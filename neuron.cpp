/*
	Artificial Intelligence & Cybernetics Research Group
	
	Algorithm
	-----------
	1. Initialize the sub-populations
	2. Evaluate the sub-populations on the two given objectives using random collaborations
	3. Rank the sub-populations and identify non-dominated individuals
	4. Assign Pareto front
	While Max_Evaluations not Reached
		For all sub-pops	
			5. Select parents and generate offspring
			6. update the sub-populations
			7. Rank Sub-populations & Identify non-dominated individuals
			8. Assign Pareto Front	
		End For
	End While
	
	
	
	Update the following before your run your experiments:
	1. Training File Name (For all objectives)
	2. Training File Size (For all objectives)
	3. Test File Name (For all objectives)
	4. Test File Size (For all objectives)
	5. No. of hidden neurons
	6. No. of experimental runs for each neuron
	 
*/
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <ctime>
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<ctype.h>

time_t TicTime;
time_t TocTime;
using namespace::std;
//type definitions for vectors
typedef vector<double> Layer;
typedef vector<double> Nodes;
typedef vector<double> Frame;
typedef vector<int> Sizes;
typedef vector<vector<double> > Weight;
typedef vector<vector<double> > Data;
typedef vector<vector<int> > Dataint;

#define usememe 
#define usememe 
#define EPSILON 1e-50
# define INF 1.0e14
# define EPS 1.0e-14
# define E  2.71828182845905
# define PI 3.14159265358979
const int LayersNumber = 3; //total number of layers.


//constants
const int culturalPopSize = 20;
const int CCPOPSIZE = 300;//population size
const int minhidden = 1;//minimum number of hidden layers
const int maxhidden = 3;//maximum number of hidden layers
const int depthbegin = 10;
const int depthend  = 10;
const int depthinc = 5;
const int fixedhidden = 4;


int maxgen =50000;//number of function evaluations




const double mintrain = 95;
const int trainsize1 = 199 ;
const int trainsize2 = 133 ;//size of training set
const int testsize1  = 199 ;
const int testsize2  = 133 ;//size of test set
const   int input =  4; //input dimensions
const int output  = 1; //output dimensions
const double MinimumError = 0.0001;//error tolerance
const int num_objectives=2;



char  * trainfile1 = "acitrain_2.txt";//training file
char  * trainfile2 = "acitrain_3.txt";
char * testfile1= "acitest_2.txt";
char * testfile2= "acitest_3.txt"; //test file


#define neuronlevel   //hiddenNeuron, weightlevel, neuronlevel,networklevel -->Decomposition Method
#define sigmoid  //tanh, sigmoid --> For positive values use sigmoid else use tanh


int row ;
int col;
int layer;
int r;
int x;
int y;

double weightdecay = 0.005;

#define rosen          // choose the function:
#define EPSILON 1e-50
#define MAXFUN 50000000  //upper bound for number of function evaluations
#define MINIMIZE 1      //set 1 to minimize and -1 to maximize
#define LIMIT 1e-20     //accuracy of best solution fitness desired
#define KIDS 8          //pool size of kids to be formed (use 2,3 or 4)
#define M 1             //M+2 is the number of parents participating in xover (use 1)
#define family 2        //number of parents to be replaced by good individuals(use 1 or 2)
#define sigma_zeta 0.1
#define sigma_eta 0.1   //variances used in PCX (best if fixed at these values)
#define  NoSpeciesCons 300
#define NPSize KIDS + 2   //new pop size
#define RandParent M+2     //number of parents participating in PCX
#define MAXRUN 10      //number of runs each with different random initial population

double d_not[CCPOPSIZE];
double seed,basic_seed;
int RUN;


/*
 	--InitialiseData--
 	Function is used to setup the dataset, input and output vectors.
 	1.The dataset vector is initialised with the data from the file
 	2.The dataset vector is then used to initialize the output/input vectors
 */
class TrainingExamples{
    friend class NeuralNetwork;
    
	protected:
       //class variables
	   Data  InputValues;
       Data  DataSet;
       Data  OutputValues;
	   char* FileName;
	   int Datapoints;
       int colSize ;
       int inputcolumnSize ;
       int outputcolumnSize ;
	   int datafilesize  ;
    
	public:
		//function declarations
		TrainingExamples()
		{
			//constructor
		} ;
		//overriden constructor
		TrainingExamples( char* File, int size, int length, int inputsize, int outputsize ){
			//initialize functions and class variables
			inputcolumnSize = inputsize ;
			outputcolumnSize = outputsize;
			datafilesize = inputsize+ outputsize;
			colSize = length;
			Datapoints= size;

			FileName = File;
			InitialiseData();
		}
		
		void printData();
		
		void InitialiseData();

};
 /*
 	--InitialiseData--
 	Function is used to setup the dataset, input and output vectors.
 	1.The dataset vector is initialised with the data from the file
 	2.The dataset vector is then used to initialize the output/input vectors
 */
void TrainingExamples:: InitialiseData()
{
	ifstream in( FileName );
    if(!in) {
		cout << endl << "failed to open file" << endl;//error message for reading from file
	}
    
	//initialise dataset vectors
	for(int r=0; r <  Datapoints ; r++)
		DataSet.push_back(vector<double> ());//create a matrix for holding the data

	for(int row = 0; row < Datapoints ; row++) {
		for(int col = 0; col < colSize ; col++)
			DataSet[row].push_back(0);//initialize dataset with 0s
	}
      // cout<<"printing..."<<endl;
    for(  int row  = 0; row   < Datapoints ; row++)
		for( int col  = 0; col  < colSize; col ++)
			in>>DataSet[row ][col];//fill dataset matrix with values from the training examples file
      //-------------------------
    //initialise intput vectors
	for(int  r=0; r < Datapoints; r++)
		InputValues.push_back(vector<double> ());

    for(int  row = 0; row < Datapoints ; row++)
		for(col = 0; col < inputcolumnSize ; col++)
			InputValues[row].push_back(0);//initialise with 0s

    for(int  row = 0; row < Datapoints ; row++)
		for(int col = 0; col < inputcolumnSize ;col++)
			InputValues[row][col] = DataSet[row ][col] ;//read values from the dataset vector
    
	//initialise output vectors
	for(int r=0; r < Datapoints; r++)
		OutputValues.push_back(vector<double> ());//create matrix

    for( int row = 0; row < Datapoints ; row++)
		for( int col = 0; col < outputcolumnSize; col++)
			OutputValues[row].push_back(0);//initialse with 0s

    for( int row = 0; row < Datapoints ; row++)
		for(int  col = 0; col <  outputcolumnSize;col++)
			OutputValues[row][col]= DataSet[row ][ col +inputcolumnSize ] ;//read values from last layer of dataset

    in.close();//close connection
 } 
/*
 	--printData--
 	Displays to the user the entire training set including all the input values and the expected output values
 	
*/
void TrainingExamples:: printData()
{
    cout<<"printing...."<<endl;
	cout<<"Entire Data Set.."<<endl;
	for(int row = 0; row < Datapoints ; row++) {
		for(int col = 0; col < colSize ; col++)
			cout<<  DataSet[row][col]<<" ";//output entire set
			cout<<endl;
	}
    
	cout<<endl<<"Input Values.."<<endl;  
    
	for(int  row = 0; row < Datapoints ; row++) {
		for( int col = 0; col < inputcolumnSize; col++)
			cout<<  InputValues[row][col]<<" ";//output only input values
			cout<<endl;
    }
    
	cout<<endl<<"Expected Output Values.."<<endl;   
   
	for( int row = 0; row < Datapoints ; row++)  {
		for( int col = 0; col <  outputcolumnSize;col++)
			cout<< OutputValues[row][col] <<" " ;//print output values
			cout<<endl;
	}
}

/*  --Layer Class--
	Layer Class is used to manage the different layers of the neural network
	It manages the weights, neuron values, bias factor and changes in weights and bais.
	Each element is a vector or vector containing other vectors forming a multi-dimensional matrix
	This allows for managing of corresponding weights and values
*/
class Layers{  

    friend class NeuralNetwork;

    protected:
		//class variables
        Weight Weights ;//neural network weights 
        Weight WeightChange;//change in weight after each iteration
        Weight H;
        Layer Outputlayer;
        Layer Bias;//keeping track of the bias factor in the network
        Layer B;
        Layer Gates;
        Layer BiasChange;//keeping track of change in Bias factor
        Layer Error;//error after each iteration

    public:
		//functions
		Layers()
        {
			//contructor method
        }

};
/*  --NeuralNetwork Class--
	This class is used to represent the neural network structure and its functionality
	It makes use of the layer class to structure the network
	
*/
class NeuralNetwork{
    friend class GeneticAlgorithmn;
    friend class CoEvolution;
    
	protected:
		//class variables
		Layers nLayer[LayersNumber];
		double Heuristic;
		int StringSize;
		Layer ChromeNeuron;
        int NumEval;
		Data Output;
        Sizes layersize;
		double NMSE;
 
  
     public:
		//class functions
        NeuralNetwork( )
        {
			//constructor
        }
		
		NeuralNetwork(Sizes layer )
		{
			layersize  = layer;
			StringSize = (layer[0]*layer[1])+(layer[1]*layer[2]) +   (layer[1] + layer[2]);
		}
		
		double Random();

		double Sigmoid(double ForwardOutput);

		double NMSError() {return NMSE;} 

		void CreateNetwork(Sizes Layersize ,TrainingExamples TraineeSamples);

		void ForwardPass(TrainingExamples TraineeSamp,int patternNum,Sizes Layersize);

		double SumSquaredError(TrainingExamples TraineeSamples,Sizes Layersize);

		void SaveLearnedData(Sizes Layersize,char* filename) ;

		double  NormalisedMeanSquaredError(TrainingExamples TraineeSamples, Sizes Layersize);

		void LoadSavedData(Sizes Layersize,char* filename) ;

		Layer  Neurons_to_chromes( );

        double  TestTrainingData(Sizes Layersize, char* filename,int  size, char* load, int inputsize, int outputsize, ofstream & out1, ofstream & out2, int objective  );

		void  ChoromesToNeurons(  Layer NeuronChrome);

		double ForwardFitnessPass(  Layer NeuronChrome,   TrainingExamples Test) ;
};

/*
	--Random--
	Is used to generate random numbers which are used to initialize weights and neurons when the network is created
*/
double NeuralNetwork::Random()//method for assigning random weights to Neural Network connections
{     
	int chance;
    double randomWeight;
    double NegativeWeight;
    chance =rand()%2;//randomise between positive and negative 

    if(chance ==0){
		randomWeight =rand()% 100;
		return randomWeight*0.05;//assign positive weight
    }

    if(chance ==1){
		NegativeWeight =rand()% 100;
		return NegativeWeight*-0.05;//assign negative weight
    }

}

/*
	--Sigmoid--
	Function to convert weighted_sum into a value between  -1 and 1
*/
double NeuralNetwork::Sigmoid(double ForwardOutput) 
{
    double ActualOutput;
	#ifdef sigmoid 
		ActualOutput = (1.0 / (1.0 + exp(-1.0 * (ForwardOutput) ) ));
	#endif
    
	#ifdef tanh
		ActualOutput  = (exp(2 * ForwardOutput) - 1)/(exp(2 * ForwardOutput) + 1);
	#endif
    return  ActualOutput;
}

/*
	--CreateNetwork--
	This function is responsible for setting the overall network structure and initially the structure with random weights, random bais factors and 0s for each neuron value.
*/
void NeuralNetwork::CreateNetwork(Sizes Layersize,TrainingExamples TraineeSamples)//create network and initialize the weights
{
       

	int end = Layersize.size() - 1;

	for(layer=0; layer < Layersize.size()-1; layer++){//go through each layer
  
        for(int  r=0; r < Layersize[layer]; r++)//go through the number of connections betwneen layers i.e the weights for the connections between layers
			nLayer[layer].Weights.push_back(vector<double> ()); //each layer will have matrix of vectors representing  the weights 

        for( int row = 0; row< Layersize[layer] ; row++)
			for( int col = 0; col < Layersize[layer+1]; col++)
				nLayer[layer].Weights[row].push_back(Random());//initialize weights to random values for all layers
     
		for(int  r=0; r < Layersize[layer]; r++)              // the weight change vector will have a vecctor(of type double) inside it for each of the elements
			nLayer[layer].WeightChange.push_back(vector<double> ()); // this will be used to keep track of the change in weights after each iteration
																	//again this depends on number of connections(weights) we have between layers
                                                                   
        for( int row = 0; row < Layersize[layer] ; row ++)
			for( col = 0; col < Layersize[layer+1]; col++)
				nLayer[layer].WeightChange[row ].push_back(0);//initialize all the elements in weighchange matrix with 0 as initially we have no change

        for( int r=0; r < Layersize[layer]; r++)
			nLayer[layer].H.push_back(vector<double> ());//create matrix

        for(int  row = 0; row < Layersize[layer] ; row ++)
			for( int col = 0; col < Layersize[layer+1]; col++)
				nLayer[layer].H[row ].push_back(0);//initialize all the elements in H with 0 for each layer
     
    }

    for( layer=0; layer < Layersize.size(); layer++){
		
		for( row = 0; row < Layersize[layer] ; row ++)
			nLayer[layer].Outputlayer.push_back(0);//initialize neurons of each layer with 0s

		for( row = 0; row < Layersize[layer] ; row ++)
			nLayer[layer ].Bias.push_back(Random());//the bias for each each layer and connection will be a random value

		for( row = 0; row < Layersize[layer] ; row ++)
			nLayer[layer ].Gates.push_back(0);//initialize gates vector with 0s


		for( row = 0; row < Layersize[layer] ; row ++)
			nLayer[layer ].B.push_back(0);//initialize with 0s

		for( row = 0; row < Layersize[layer] ; row ++)//for each connection we will also keep track of change in bias factor
			nLayer[layer ].BiasChange.push_back(0);//initially it will be all 0

		for( row = 0; row < Layersize[layer] ; row ++)
			nLayer[layer ].Error.push_back(0);// intialize error vector for each layer with 0s
    }
    
    for( r=0; r < TraineeSamples.Datapoints; r++)
        Output.push_back(vector<double> ());//the output vector will have nested vectors(of type double) inside it forming a matrix..depending on the number of training examples
    
	for( row = 0; row< TraineeSamples.Datapoints ; row++)
        for( col = 0; col < Layersize[end]; col++)
			Output[row].push_back(0);// intialize all the rows in the output vector with 0s
	for( row = 0; row < StringSize ; row ++)
        ChromeNeuron.push_back(0); //intialise the ChromeNeuron vector with 0s

}

/*  --Forward Pass--
	1. Feed the network values from the training set through the input layer
	2. Apply the given weights to the input values for each layer of the network
	3. Apply the bias factor to the computed weighted sum
	4. Convert the resulting values using the sigmoid function to a value between -1 and 1
	3. Once the final layer is reached output the computed values from the network.
*/
void NeuralNetwork::ForwardPass(TrainingExamples TraineeSamples,int patternNum,Sizes Layersize)
{
   //declaring essential variables
	double WeightedSum = 0;
    double ForwardOutput;//to hold output value between -1 and 1
	int end = Layersize.size() - 1; //know the last layer

    for(int row = 0; row < Layersize[0] ; row ++)
		nLayer[0].Outputlayer[row] = TraineeSamples.InputValues[patternNum][row];//initializing first layer with values from training examples
 


    for(int layer=0; layer < Layersize.size()-1; layer++){//go layer by layer calculating forward pass...i.e multiplying neuron values by the connection weights

		for(int y = 0; y< Layersize[layer+1]; y++) {
			for(int x = 0; x< Layersize[layer] ; x++){
				WeightedSum += (nLayer[layer].Outputlayer[x] * nLayer[layer].Weights[x][y]);//multiplying weights from the weights matrix by the value in the corresponding neuron  to determine value for neuron in next layer

				ForwardOutput = WeightedSum - nLayer[layer+1].Bias[y]; //subtracting the bias weight from the weighted sum giving the new value for the neuron in the next layer to which these weights are connected
			}

			nLayer[layer+1].Outputlayer[y] = Sigmoid(ForwardOutput);//convert the weighted sum to a value between 1 and -1 which will be the new value in the neuron

			WeightedSum = 0;//reset weighted sum to 0 for next neuron
		}
		WeightedSum = 0;//reset weighted sum to 0 for next layer
	}//end layer

   //--------------------------------------------
   for(int output= 0; output < Layersize[end] ; output ++){
		Output[patternNum][output] = nLayer[end].Outputlayer[output];//setting the values for the final output in the last layer.
  
	}

 }
 


/*
	--SumSquardedError--
	1.Calculated error 
	2.Square the error
	3.Add to cumulative overall error and repeat steps 1 to 3 for all output values
	4. Square root (sum/(no. of training samples * no. of neurons in output layer)) and return result-RMSE
	
*/
double NeuralNetwork::SumSquaredError(TrainingExamples TraineeSamples,Sizes Layersize)
{   
	int end = Layersize.size() - 1;
    double Sum = 0;
    double Error=0;
    double ErrorSquared = 0;
    
	for(int pattern = 0; pattern< TraineeSamples.Datapoints ; pattern++){
		
		for(int output = 0; output < Layersize[end]; output++) {
			Error = fabs(TraineeSamples.OutputValues[pattern][output]) - fabs(Output[pattern][output]); 
			ErrorSquared += (Error * Error);//square the error
		}
    
		Sum += (ErrorSquared);//add to cumulative error
        ErrorSquared = 0;//set error squared variable to 0 for next layer
	}
	return sqrt(Sum/TraineeSamples.Datapoints*Layersize[end]); //return square root of sum / (no. of training samples * no. of neurons in output layer)	
}

/*
	--Normalised Mean Squared Error--
	
*/
double NeuralNetwork::NormalisedMeanSquaredError(TrainingExamples TraineeSamples,Sizes Layersize)
{   
	//variable declaration
	int end = Layersize.size() - 1;
    double Sum = 0;
    double Sum2 = 0;
    double Error=0;
    double ErrorSquared = 0;
    double Error2=0;
    double ErrorSquared2 = 0;
    double meany = 0;

    for(int pattern = 0; pattern< TraineeSamples.Datapoints ; pattern++){// go through all data points
		for(int input = 0; input < Layersize[0]; input++) { //input layer
			meany+= fabs(TraineeSamples.InputValues[pattern][input]); //calculate mean
		}
		meany/= (Layersize[0]*TraineeSamples.Datapoints); 
        
		for(int output = 0; output < Layersize[end]; output++) { //output layer
			Error2 = fabs(TraineeSamples.OutputValues[pattern][output] ) - meany; //error 
			Error = fabs(TraineeSamples.OutputValues[pattern][output]) - fabs(Output[pattern][output]);// error between expected and actual output
			ErrorSquared += (Error * Error);//square the error
			Sum += (ErrorSquared);//add to cumulative error
			ErrorSquared = 0; 
			ErrorSquared2 += (Error2 * Error2);//square the value
        }
		
		meany = 0;
        Sum += (ErrorSquared);
        Sum2 += (ErrorSquared2);
        ErrorSquared = 0;
        ErrorSquared2 = 0;
	}

	return Sum/Sum2;//normalise
}



/*
	--SaveLearnedData--
	Save the network weights and bias values which were able to achieve optimal results to file
*/
void NeuralNetwork::SaveLearnedData(Sizes Layersize, char* filename)//save data to file
{

	ofstream out;
	out.open(filename);
	if(!out) {
		cout << endl << "failed to save file" << endl;//error in writing to file
		return;
    }
	
    for(int layer=0; layer < Layersize.size()-1; layer++){//ouput weights
        for(int row  = 0; row <Layersize[layer] ; row ++){
			for(int col = 0; col < Layersize[layer+1]; col++)
				out<<nLayer[layer].Weights[row ][col]<<" ";
				out<<endl;
        }
        out<<endl;//blank line
    }
 
  // output bias.
	for(int  layer=1; layer < Layersize.size(); layer++){
		for(int y = 0 ; y < Layersize[layer]; y++) {
			out<<	nLayer[layer].Bias[y]<<"  ";
			out<<endl<<endl;
        }
	    out<<endl;
    }
    
	out.close();//data saved--close connection

	return;
}

/*
	--LoadSavedData--
	Load the network weights and bias values which were able to achieve optimal results from file
*/
void NeuralNetwork::LoadSavedData(Sizes Layersize,char* filename)//load saved data from file
{
 	ifstream in(filename);
    if(!in) {
		cout << endl << "failed to save file" << endl;//error reading from file
		return;
    }
    
	for(int layer=0; layer < Layersize.size()-1; layer++)//read weights
		for(int row  = 0; row <Layersize[layer] ; row ++)
			for(int col = 0; col < Layersize[layer+1]; col++)
				in>>nLayer[layer].Weights[row ][col];


	for( int layer=1; layer < Layersize.size(); layer++)//read bias
		for(int y = 0 ; y < Layersize[layer]; y++)
			in>>	nLayer[layer].Bias[y] ;

	in.close();
	cout << endl << "data loaded for testing" << endl;//data read...close connection 
	return;
 }

/*
	--TestTrainingData--
	Test the trained network with testing data
*/     
double NeuralNetwork::TestTrainingData(Sizes Layersize, char* filename,int  size, char* load, int inputsize, int outputsize, ofstream & out1, ofstream & out2, int objective  )
{
    //variable declaration
    bool valid;
    double count = 1;
    int total;
    double accuracy;

	int end = Layersize.size() - 1;
	//load testing data
	TrainingExamples Test(load,size,inputsize+outputsize ,   inputsize,  outputsize ); 
	//initialize network
	CreateNetwork(Layersize,Test);
	//load saved training data
	LoadSavedData(Layersize,filename); 
	//get testing values and do a forward pass 
	for(int pattern = 0;pattern < size ;pattern++){
		ForwardPass(Test,pattern,Layersize);
	}
	//write to file the desired output, actual output and error
	for(int pattern = 0; pattern< size; pattern++){
		for(int output = 0; output < Layersize[end]; output++) {
			if(objective==1){//output prediction vs actual for obj1
				out1<< Output[pattern][output]   <<" "<<Test.OutputValues[pattern][output]<<" "<<fabs( fabs(Test.OutputValues[pattern][output] )-fabs    (Output[pattern][output]))<<endl;

			}else{//output prediction vs actual for obj2
				out2<< Output[pattern][output]   <<" "<<Test.OutputValues[pattern][output]<<" "<<fabs( fabs(Test.OutputValues[pattern][output] )-fabs    (Output[pattern][output]))<<endl;
      			}
		}  
	}
	out2<<endl;
	out2<<endl; 
	//output rmse and nmse to file
	accuracy = SumSquaredError(Test,Layersize);  
	if(objective==1){
		out1<<" RMSE:  " <<accuracy<<endl;
		cout<<"RMSE: " <<accuracy<<" %"<<endl;
		NMSE =  NormalisedMeanSquaredError(Test,Layersize);  
		out1<<" NMSE:  " <<NMSE<<endl;
	}else{
		out2<<" RMSE:  " <<accuracy<<endl;
		cout<<"RMSE: " <<accuracy<<" %"<<endl;
		NMSE =  NormalisedMeanSquaredError(Test,Layersize);  
		out2<<" NMSE:  " <<NMSE<<endl;
	}
	return accuracy;

}

 

/*
	--Neurons_to_chromes--
	Convert neurons to chromes which can be processed by the genetic algorithm
*/
Layer NeuralNetwork::  Neurons_to_chromes(  )
{
	//variable declaration
    int gene = 0;
    Layer NeuronChrome(StringSize);
	int layer = 0;
	 
	//if neuronlevel decomposition is selected 
	#ifdef neuronlevel
	
	for(int neu = 0; neu < layersize[1]; neu++ ){

		for( int row = 0; row<  layersize[layer] ; row++){
		    NeuronChrome[gene]=nLayer[layer].Weights[row][neu]; //convert weights to chromes for hidden layer
			gene++;   
		}

		NeuronChrome[gene] = nLayer[layer+1 ].Bias[neu];//convert bias to chromes for hidden layer
		gene++;//increase iteration
	}

	layer = 1;
	
	for(int neu = 0; neu < layersize[2]; neu++ ){

		for( int row = 0; row<  layersize[layer] ; row++){
			NeuronChrome[gene]  = nLayer[layer].Weights[row][neu];//convert weights to chromes for output layer
			gene++;   
		}

		NeuronChrome[gene] = nLayer[layer+1 ].Bias[neu] ;// convert bias to chromes for output layer
		gene++; //increase iteration
	}
	#endif 

	
	#ifdef networklevel
    for(int layer=0; layer <  layersize.size()-1; layer++){
        for( row = 0; row<  layersize[layer] ; row++){
			for( col = 0; col <  layersize[layer+1]; col++)  {
				NeuronChrome[gene] =  nLayer[layer].Weights[row][col];//convert weights to chromes for all layers
				gene++; //increase iteration
			} 
		}
	}
      
	for(int layer=0; layer <  layersize.size() ; layer++){
		for( row = 0; row <  layersize[layer] ; row ++) {
			NeuronChrome[gene]  =  nLayer[layer ].Bias[row]; // convert bias to chromes for all layers
			gene++; //increase iteration
		}
	}
	#endif

	#ifdef weightlevel
	for(int layer=0; layer <  layersize.size()-1; layer++){
        for( row = 0; row<  layersize[layer] ; row++){
			for( col = 0; col <  layersize[layer+1]; col++)  {
				NeuronChrome[gene] =  nLayer[layer].Weights[row][col];//convert weights to chromes for all layers
				gene++; //increase iteration
			} 
		}
	}
    
	for(int layer=0; layer <  layersize.size() ; layer++){
		for( row = 0; row <  layersize[layer] ; row ++) {
			NeuronChrome[gene]  =  nLayer[layer ].Bias[row];//convert bias to chromes for all layers
			gene++; //increase iteration
		}
	}
	#endif

	return   NeuronChrome;//return genes
}

/*
	--ChromesToNeurons--
	Convert the chromes back to the neural network structure
*/

void NeuralNetwork:: ChoromesToNeurons(  Layer NeuronChrome)
{
	StringSize = NeuronChrome.size();
 	int layer = 0;
	int gene = 0;
	#ifdef neuronlevel
		for(int neu = 0; neu < layersize[1]; neu++ ){

			for( int row = 0; row<  layersize[layer] ; row++){
			    nLayer[layer].Weights[row][neu]=  NeuronChrome[gene]  ;//convert genes back to neurons and weights for hidden layer
			    gene++;   
			}

		    nLayer[layer+1 ].Bias[neu] =   NeuronChrome[gene]  ;//convert genes back to bias for hidden layer
		    gene++;
		}

		layer = 1;

		for(int neu = 0; neu < layersize[2]; neu++ ){

			for( int row = 0; row<  layersize[layer] ; row++){
			    nLayer[layer].Weights[row][neu]=  NeuronChrome[gene]  ; //convert genes back to neurons and weights for output layer
				gene++;   
			}

			nLayer[layer+1 ].Bias[neu] =   NeuronChrome[gene]  ;//convert genes back to bais for output layer
			gene++;
		}


	#endif

	#ifdef hiddenNeuron
		for(int neu = 0; neu < layersize[1]; neu++ ){

			for( int row = 0; row<  layersize[layer] ; row++){
			    nLayer[layer].Weights[row][neu]=  NeuronChrome[gene]  ; //convert genes back to neurons and weights for hidden layer
			    gene++;   
			}

		    nLayer[layer+1 ].Bias[neu] =   NeuronChrome[gene]  ;
		    gene++;

		    for( int row = 0; row<  layersize[layer+2] ; row++){
		        nLayer[layer+1].Weights[neu][row]=  NeuronChrome[gene]  ; //convert genes back to bais for hideen layer
                gene++;
			}

		 }


	#endif
	
	#ifdef weightlevel

		for(int layer=0; layer <  layersize.size()-1; layer++){
			for( row = 0; row<  layersize[layer] ; row++){
				for( col = 0; col <  layersize[layer+1]; col++)  {
					nLayer[layer].Weights[row][col]=  NeuronChrome[gene]  ; //convert genes back to neurons and weights for all layers
					gene++; 
				} 
			}
		}
   
		for(int layer=0; layer <  layersize.size() ; layer++){
			for( row = 0; row <  layersize[layer] ; row ++) {
				nLayer[layer ].Bias[row] =   NeuronChrome[gene]  ; //convert genes back to bias for all layers
				gene++; 
			}
		}
	#endif


	#ifdef networklevel

		for(int layer=0; layer <  layersize.size()-1; layer++){
			for( row = 0; row<  layersize[layer] ; row++){
				for( col = 0; col <  layersize[layer+1]; col++)  {
					nLayer[layer].Weights[row][col]=  NeuronChrome[gene]  ; //convert genes back to neurons and weights for all layers
					gene++; 
				} 
			}
		}
	  
		for(int layer=0; layer <  layersize.size() ; layer++){
			for( row = 0; row <  layersize[layer] ; row ++) {
				nLayer[layer ].Bias[row] =   NeuronChrome[gene]  ; //convert genes back to bias for all layers
				gene++; 
			}
		}
	#endif

}
/*
	--ForwardFitnessPass--
	Calculate the fitness of the network with the test data
*/
double NeuralNetwork::ForwardFitnessPass(  Layer NeuronChrome,   TrainingExamples Test)
{
	//convert the chromes back to neurons
    ChoromesToNeurons( NeuronChrome);
	NumEval++;//number of function evaluations

	double SumErrorSquared = 0;
	bool Learn=true;

    for(int pattern = 0; pattern < Test.InputValues.size(); pattern++)
    {
		ForwardPass( Test,pattern,layersize);// do forward pass on the input values and calculate output for the test data

    }

	SumErrorSquared = SumSquaredError(Test,layersize);//calculate sum squared error from the output on the test data

    if(Learn == false )
        return -1;

	return  SumErrorSquared ; //return error

}

/*
	--Individual Class--
	Represents a particular chromozone
*/
class Individual{

    public: //class variables
		Layer Chrome;
		double Fitness[num_objectives];
		Layer BitChrome;
		int rank;

	public://class functions
        Individual()
        {
			for(int i=0; i<num_objectives; i++){
				Fitness[i]=0;
			}
        }
        void print();


};
typedef vector<double> Nodes;

/*
	--Genetic Algorithm--
	Used to evolve a population in order to achieve a set which has an acceptable fitness level
*/
class GeneticAlgorithmn
{
    public://class variables
		int PopSize;
		vector<Individual> Population;
		Sizes TempIndex ;
		vector<Individual> NewPop; 
     	Sizes mom ;
     	Sizes list;
		int MaxGen;
		int NumVariable;
		double BestFit;
		int BestIndex;
		int NumEval;
		int  kids;

    public: //class functions
		GeneticAlgorithmn(int stringSize )
        {
			NumVariable = stringSize;
			NumEval=0;
			BestIndex = 0;
        }
		
        GeneticAlgorithmn()
		{
            BestIndex = 0;
        }

		double Fitness() {return BestFit;}

		double  RandomWeights();

		double  RandomAddition();

		double rand_normal(double mean, double stddev);

		int GenerateNewPCX(int pass, double Mutation, int depth);
		
		void  InitilisePopulation(int popsize);

		double  modu(double index[]);

		double  innerprod(double Ind1[],double Ind2[]);

		double RandomParents();

		void  my_family();   //here a random family (1 or 2) of parents is created who would be replaced by good individuals
		
		void sort();

};

/*
	--RandomWeights--
	Generate random number  between -5 and 5(double)
*/
double GeneticAlgorithmn::RandomWeights()
{
    int chance;
    double randomWeight;
    double NegativeWeight;
    chance =rand()%2;

    if(chance ==0){
		randomWeight =rand()%  100000;
		return randomWeight*0.00005;
    }

    if(chance ==1){
		NegativeWeight =rand()% 100000;
		return NegativeWeight*-0.00005;
    }
}

/*
	--rand_normal--
	Box Muller Random Numbers
*/
double GeneticAlgorithmn::rand_normal(double mean, double stddev) { 
	
	static double n2 = 0.0;
    static int n2_cached = 0;
   
   if (!n2_cached) {
       // choose a point x,y in the unit circle uniformly at random
		double x, y, r;
		do {
			//  scale two random integers to doubles between -1 and 1
			x = 2.0*rand()/RAND_MAX - 1;
			y = 2.0*rand()/RAND_MAX - 1;

			r = x*x + y*y;
		} while (r == 0.0 || r > 1.0);

       
		// Apply Box-Muller transform on x, y
		double d = sqrt(-2.0*log(r)/r);
		double n1 = x*d;
		n2 = y*d;

       // scale and translate to get desired mean and standard deviation
		double result = n1*stddev + mean;

        n2_cached = 1;
        return result;
        
    } else {
	
		n2_cached = 0;
		return n2*stddev + mean;
		
    }
}

/*
	--RandomAddition--
	Generate random number  between -0.9 and 0.9 (type double)
*/

double GeneticAlgorithmn::RandomAddition()
{
    int chance;
    double randomWeight;
    double NegativeWeight;
    chance =rand()%2;

    if(chance ==0){
		randomWeight =rand()% 100;
		return randomWeight*0.009;
    }

    if(chance ==1){
		NegativeWeight =rand()% 100;
		return NegativeWeight*-0.009;
    }

}

/*
	--InitializsePopulation--
	Intialize the vectors used in algorithm,
*/
void GeneticAlgorithmn::InitilisePopulation(int popsize)
{
	//variable declaration
	double x, y;
	Individual Indi ;
	NumEval=0;
    BestIndex = 0;
    PopSize = popsize;
	
	for (int i = 0; i < PopSize; i++){ 
        TempIndex.push_back(0); //initialize with 0s
        mom.push_back(0); //initialize female parent 
	}

	for (int i = 0; i < NPSize; i++){  	
		list.push_back(0); //initialize with 0s
	}

	for(int row = 0; row < PopSize  ; row++) 
        Population.push_back(Indi); //population made up of individuals

	for(int row = 0; row < PopSize  ; row++) {
		for(int col = 0; col < NumVariable ; col++){
  
			Population[row].Chrome.push_back(RandomWeights());//initialize population with random weights
		}
	}

	for(int row = 0; row < NPSize  ; row++) 
		NewPop.push_back(Indi);//new population made up of individuals

	for(int row = 0; row < NPSize  ; row++) {
		for(int col = 0; col < NumVariable ; col++)
			NewPop[row].Chrome.push_back(0);//initialise new population with 0s

	}
     
}

/*
	--my_family--
	Here a random family (1 or 2) of parents is created who would be replaced by good individuals
*/
void GeneticAlgorithmn:: my_family()   
{
	int i,j,index; //variables
	int swp;
	double u;

	for(i=0;i<PopSize;i++)
		mom[i]=i;

	for(i=0;i<family;i++)
    {
		index = (rand()%PopSize) +i; //randomly get index value which corresponds to an individual from the population

		if(index>(PopSize-1)) index=PopSize-1; //make sure index is not outside population range
		swp=mom[index];
		mom[index]=mom[i];
		mom[i]=swp; //swap values
    }
}

/*
	--sort--
	Arrange individuals in order of their performance on obj1
	
*/

void GeneticAlgorithmn::sort()
{
	int i,j, temp;
	double dbest;

	for (i=0;i<(kids+family);i++) list[i] = i;

	if(MINIMIZE)
		for (i=0; i<(kids+family-1); i++)
		{
			dbest = NewPop[list[i]].Fitness[0];
			for (j=i+1; j<(kids+family); j++)
			{
				if(NewPop[list[j]].Fitness[0] < dbest) //check for best
				{
					dbest = NewPop[list[j]].Fitness[0];//update best
					temp = list[j];
					list[j] = list[i];
					list[i] = temp;
				}
			}
		}
	else
		for (i=0; i<(kids+family-1); i++)
		{
			dbest = NewPop[list[i]].Fitness[0];
			for (j=i+1; j<(kids+family); j++)
			{
				if(NewPop[list[j]].Fitness[0] > dbest)//check for best
				{
					dbest = NewPop[list[j]].Fitness[0];//update best
					temp = list[j];
					list[j] = list[i];
					list[i] = temp;
				}
			}
      }
}

/*
	--modu--
	Calculate modulus
*/
double GeneticAlgorithmn::  modu(double index[])
{
	//variables
	int i;
	double sum,modul;
	sum=0.0;
	
	for(i=0;i<NumVariable ;i++)
		sum+=(index[i]*index[i]);

	modul=sqrt(sum);
	
	return modul;
}

/*
	--innerprod--
	calculates the inner product of two vectors
*/
double GeneticAlgorithmn::  innerprod(double Ind1[],double Ind2[])
{
	//variables
	int i;
	double sum;
	sum=0.0;

	for(i=0;i<NumVariable ;i++)
		sum+=(Ind1[i]*Ind2[i]);

	return sum;//return inner_product
}

/*
	--GenerateNewPCX--
	Generate new population
*/
int GeneticAlgorithmn::GenerateNewPCX(int pass,  double Mutation, int depth)
{
	//variable declaration
	int i,j,num,k;
	double Centroid[NumVariable];
	double tempvar,tempsum,D_not,dist;	
	double tempar1[NumVariable];
	double tempar2[NumVariable];
	double D[RandParent];
	double d[NumVariable];
	double diff[RandParent][NumVariable];
	double temp1,temp2,temp3;
	int temp;

	for(i=0;i<NumVariable;i++)
		Centroid[i]=0.0;//initialize centroid with 0

	// centroid is calculated here
	for(i=0;i<NumVariable;i++)
    {
		for(j=0;j<RandParent;j++)
			Centroid[i]+=Population[TempIndex[j]].Chrome[i];

		Centroid[i]/=RandParent;
    }
  
	// calculate the distace (d) from centroid to the index parent arr1[0]
	// also distance (diff) between index and other parents are computed
	for(j=1;j<RandParent;j++)
    {
		for(i=0;i<NumVariable;i++)
		{
			if(j == 1)
			d[i]=Centroid[i]-Population[TempIndex[0]].Chrome[i];
			diff[j][i]=Population[TempIndex[j]].Chrome[i]-Population[TempIndex[0]].Chrome[i];
		}
		if (modu(diff[j]) < EPSILON)
		{
			cout<< "RUN Points are very close to each other. Quitting this run   " <<endl;

			return (0);
		}

		if (isnan(diff[j][i])  )
		{
			cout<< "`diff nan   " <<endl;
            diff[j][i] = 1;
			return (0);
		} 


    }
	dist=modu(d); // modu calculates the magnitude of the vector

	if (dist < EPSILON)
    {
		cout<< "RUN Points are very close to each other. Quitting this run    " <<endl;

		return (0);
    }

	// orthogonal directions are computed (see the paper)
	for(i=1;i<RandParent;i++)
    {
		temp1=innerprod(diff[i],d);
		if((modu(diff[i])*dist) == 0){
			cout<<" division by zero: part 1"<<endl;
			temp2=temp1/(1);
        }
		else{
			temp2=temp1/(modu(diff[i])*dist);
		}
		temp3=1.0-pow(temp2,2.0);
		D[i]=modu(diff[i])*sqrt(temp3);
	}

	D_not=0;
	for(i=1;i<RandParent;i++)
		D_not+=D[i];

	D_not/=(RandParent-1); //this is the average of the perpendicular distances from all other parents (minus the index parent) to the index vector

	// Next few steps compute the child, by starting with a random vector
	for(j=0;j<NumVariable;j++)
    {
    	tempar1[j] = rand_normal(0, D_not*sigma_eta);
		tempar2[j]=tempar1[j];
    }

	for(j=0;j<NumVariable;j++)
    {
		if ( pow(dist,2.0)==0){
			cout<<" division by zero: part 2"<<endl;
			tempar2[j] = tempar1[j]-((innerprod(tempar1,d)*d[j])/1);
		}
      else
		tempar2[j] = tempar1[j]-((innerprod(tempar1,d)*d[j])/pow(dist,2.0));
    }

	for(j=0;j<NumVariable;j++)
		tempar1[j]=tempar2[j];

	for(k=0;k<NumVariable;k++)
		NewPop[pass].Chrome[k]=Population[TempIndex[0]].Chrome[k]+tempar1[k];
	
	tempvar=    rand_normal(0, sigma_zeta);
         

	for(k=0;k<NumVariable;k++){ 
		NewPop[pass].Chrome[k] += (tempvar*d[k]);
	}

	double random = rand()%10;

	Layer Chrome(NumVariable);

	for(k=0;k<NumVariable;k++){
		if(!isnan(NewPop[pass].Chrome[k] )){
			Chrome[k]=NewPop[pass].Chrome[k] ;
		}
		else 
			NewPop[pass].Chrome[k] =  RandomAddition();
	}

	return (1);
}

/*
	--RandomParents--
	Select two parents. First one will be the one with the best fitness within the population and the second one is randomly selected
*/
double GeneticAlgorithmn::  RandomParents()
{
	//variable declaration
	int i,j,index;
	int swp;
	double u;
	int delta;

	for(i=0;i<PopSize;i++)
		TempIndex[i]=i;

	swp=TempIndex[0];
	TempIndex[0]=TempIndex[BestIndex];  // best is always included as a parent and is the index parent
	                       // this can be changed for solving a generic problem
	TempIndex[BestIndex]=swp;

	for(i=1;i<RandParent;i++)  // shuffle the other parents i.e randomly select the other parent
	{
	  
		index=(rand()%PopSize)+i;//randomly generate index

	    if(index>(PopSize-1)) index=PopSize-1;//ensure its within the correct range
		
		swp=TempIndex[index]; 
	    TempIndex[index]=TempIndex[i];
	    TempIndex[i]=swp;//swap
	}


}

void TIC( void )
{ TicTime = time(NULL); }

void TOC( void )
{ TocTime = time(NULL); }

double StopwatchTimeInSeconds()
{ return difftime(TocTime,TicTime); }


typedef vector<GeneticAlgorithmn> GAvector;
/*
	--Table--
*/
class Table{
	    public:
			Layer   SingleSp ;
};

typedef vector<Table> TableVector;

/*
	--CoEvolution--
*/
class CoEvolution :  public  NeuralNetwork,   public virtual TrainingExamples,      public   GeneticAlgorithmn  
{

	public: //class variables
		int  NoSpecies;
		GAvector Species ;
		TableVector  TableSp;
		int PopSize;
		vector<bool> NotConverged;
		Sizes SpeciesSize;
		Layer   Individual;
		Layer  BestIndividual;
		double bestglobalfit;
		Data TempTable;
		int TotalEval;
		int TotalSize;
		Dataint pfront;
		int SingleGAsize;
		int flag;
		double Train;
        double Test;
		int kid;

		//constructor
		CoEvolution(){

		}
		//class functions
		void InitializeSpecies(int popsize);
		
		void EvaluateSpecies(NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2, ofstream &out5);

		void GetBestTable(int sp);

		void Join();

		void  RankPopulation(int s);

		void  Nondominance(int s, int ind1, int ind2);

		void find_parents(int s,NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2);

		void EvalNewPop(int pass, int s,NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2);

		void rep_parents(int s,NeuralNetwork network,TrainingExamples Sample1,TrainingExamples Sample2);

		void EvolveSubPopulations(int repetitions,double h, NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2,double mutation,int depth, ofstream &out5);

};

/*
	--InitializeSpecies--
	Initialize the sub-populations in the CC framework
*/
void  CoEvolution:: InitializeSpecies(int popsize)
{       //variable declaration 
		PopSize = popsize;
		GAvector SpeciesP(NoSpecies); //species of type Genetic Algorithm 
        Species = SpeciesP;
        
		for( int Sp = 0; Sp < NoSpecies; Sp++){
	 	    NotConverged.push_back(false);
	 	}

		basic_seed=0.4122;
		RUN =2;
		seed=basic_seed+(1.0-basic_seed)*(double)((RUN)-1)/(double)MAXRUN;
		if(seed>1.0) printf("\n warning!!! seed number exceeds 1.0");
		
		for(int i=0; i<NoSpecies; i++){
			pfront.push_back(vector<int>());

		}
		
		TotalSize = 0;
		for( int row = 0; row< NoSpecies ; row++)
			TotalSize+= SpeciesSize[row]; 

		for( int row = 0; row< TotalSize ; row++)
		    Individual.push_back(0);//initialize with 0s


		for( int s =0; s < NoSpecies; s++){
                 
			Species[s].NumVariable= SpeciesSize[s];
			Species[s].InitilisePopulation(popsize);//initialize species with 0s...that is the individuals to 0s
		}

	    TableSp.resize(NoSpecies);//resize to number of sub-pops--- to store the combined species from each of the sub-pops

        for( int row = 0; row< NoSpecies ; row++)
	        for( int col = 0; col < SpeciesSize[row]; col++)
	         	TableSp[row].SingleSp.push_back(0); //resize to the size of the species in each sub-pop
}


/*
	--GetBestTable--
	Populate TableSp with best species
	
*/
void  CoEvolution:: GetBestTable(int CurrentSp)
{
		int Best;

		for(int sN = 0; sN < CurrentSp ; sN++){
			Best= Species[sN].BestIndex;//best index

			for(int s = 0; s < SpeciesSize[sN] ; s++)
				TableSp[sN].SingleSp[s] = Species[sN].Population[Best].Chrome[s];//get best set of chromes
		}

		for(int sN = CurrentSp; sN < NoSpecies ; sN++){
			Best= Species[sN].BestIndex;
			
			for(int s = 0; s < SpeciesSize[sN] ; s++)
				TableSp[sN].SingleSp[s]= Species[sN].Population[Best].Chrome[s];//update
		}
}

/*
	--Join--
	concatenate the individual to be evaluated with the best individuals from other subpops 
*/
void   CoEvolution:: Join()
{
	int index = 0;

	for( int row = 0; row< NoSpecies ; row++){
		for( int col = 0; col < SpeciesSize[row]; col++){
			Individual[index] =  TableSp[row].SingleSp[col];	//join the species to form one individual	
			index++;
		}
	}
}

 
/*
	--EvaluateSpecies--
*/
void    CoEvolution:: EvaluateSpecies(NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2,ofstream &out5 )
{	
	
	for( int SpNum =0; SpNum < NoSpecies; SpNum++){
		GetBestTable(SpNum);//get best individuals from other subpops

		//---------make the first individual in the population the best

		for(int i=0; i < Species[SpNum].NumVariable; i++)			
			TableSp[SpNum].SingleSp[i] = Species[SpNum].Population[0].Chrome[i]; //insert individual to be evaluated into the complete solution

		Join();//concatenate the individual to be evaluated with the best individuals from other subpops 
		Species[SpNum].Population[0].Fitness[0] =  network.ForwardFitnessPass(Individual, Sample1);//evaluate fitness of first individual on obj1
		Species[SpNum].Population[0].Fitness[1] =  network.ForwardFitnessPass(Individual, Sample2);//evaluate fitness of first individual on obj2
		TotalEval++;
		Species[SpNum].BestFit = Species[SpNum].Population[0].Fitness[0];//set first individual as fittest
		Species[SpNum].BestIndex = 0;
			 
			 //------------do for the rest
		for( int PIndex=0; PIndex< PopSize; PIndex++ ){
			for(int i=0; i < Species[SpNum].NumVariable; i++)
				TableSp[SpNum].SingleSp[i]  = Species[SpNum].Population[PIndex].Chrome[i]; //insert individual to be evaluated into the complete solution
			
			Join();//concatenate the individual to be evaluated with the best individuals from other subpops 
			Species[SpNum].Population[PIndex].Fitness[0] = network.ForwardFitnessPass(Individual, Sample1);//evaluate fitness of individual on obj1
			Species[SpNum].Population[PIndex].Fitness[1] = network.ForwardFitnessPass(Individual, Sample2);//evaluate fitness of individual on obj2
			TotalEval++;
		   
        }
		int counter=0;
		RankPopulation(SpNum);//rank subpop
		out5<<"pop: "<< SpNum <<endl<<endl<<endl;
		counter=0;
		for(int j=0; j<PopSize; j++){
			
			if(Species[SpNum].Population[j].rank==0){//check if individual is non-dominated
				counter++;
				pfront[SpNum].push_back(j); //insert into pareto front
				for(int x=0; x<2; x++){
				
						
					out5<<"Individual "<<j<<"  Fitness "<<x<<": "<<Species[SpNum].Population[j].Fitness[x]<<endl;//output fitness of non-dominated individual
				}
			}
			
		}
		int x=rand()%pfront[SpNum].size(); //chose random index from set of non-dominated solutions
		Species[SpNum].BestIndex=pfront[SpNum][x];//update fittest		
		Species[SpNum].BestFit=Species[SpNum].Population[Species[SpNum].BestIndex].Fitness[0];
		out5<<"Number of non-dominated individuals: "<<counter<<endl;//output no. of non-dominated individuals for each subpop
		counter=0;
		cout<<SpNum<<" -- "<<endl;
    }

	
}

/*
	--find_parents--
	Here the parents to be replaced are added to the temporary sub-population to assess 
	their goodness against the new solutions formed which will be the basis of whether they should be kept or not
*/
void CoEvolution::find_parents(int s, NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2)   
{
	int i,j,k;
	double u,v;

	Species[s].my_family();

	for(j=0;j<family;j++)
    {
    	Species[s].NewPop[Species[s].kids+j].Chrome = Species[s].Population[Species[s].mom[j]].Chrome;//add parents to new population

		GetBestTable(s);//get best individuals from other subpops
		for(int i=0; i < Species[s].NumVariable; i++)
			TableSp[s].SingleSp[i] =   Species[s].NewPop[Species[s].kids+j].Chrome[i]; //insert individual to be evaluated into the complete solution
		
		Join();//concatenate the individual to be evaluated with the best individuals from other subpops 

		Species[s].NewPop[Species[s].kids+j].Fitness[0] =  network.ForwardFitnessPass(Individual, Sample1);//evaluate fitness on obj1
		Species[s].NewPop[Species[s].kids+j].Fitness[1] =  network.ForwardFitnessPass(Individual, Sample2);//evaluate fitness on obj2
		TotalEval++;
    }
}

/*
	--EvalNewPop--
	Evaluate fitness of new sub-populations
*/
void CoEvolution::EvalNewPop(int pass, int s,NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2)
{
	GetBestTable(s);//get best individuals from other subpops
	for(int i=0; i < Species[s].NumVariable; i++)
		TableSp[s].SingleSp[i] = 	Species[s].NewPop[pass].Chrome[i];//insert individual to be evaluated into the complete solution
    
	Join();//concatenate the individual to be evaluated with the best individuals from other subpops 
	Species[s].NewPop[pass].Fitness[0] =  network.ForwardFitnessPass(Individual, Sample1);//evaluate fitness on obj1
	Species[s].NewPop[pass].Fitness[1] =  network.ForwardFitnessPass(Individual, Sample2); //evaluate fitness on obj2
	TotalEval++;
}




/*
	--rep_parents--
*/
void CoEvolution::rep_parents(int s,NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2)   //here the best (1 or 2) individuals replace the family of parents
{
	int i,j;
	for(j=0;j<family;j++)
    {
		Species[s].Population[Species[s].mom[j]].Chrome = Species[s].NewPop[Species[s].list[j]].Chrome;//update population with new species

		GetBestTable(s);//get best individuals from other subpops

		for(int i=0; i < Species[s].NumVariable; i++)
			TableSp[s].SingleSp[i] =   Species[s].Population[Species[s].mom[j]].Chrome[i];  //insert individual to be evaluated into complete solution
		
		Join();//concatenate the individual to be evaluated with the best individuals from other subpops 

		Species[s].Population[Species[s].mom[j]].Fitness[0] =  network.ForwardFitnessPass(Individual, Sample1);//evaluate fitness for obj1
		Species[s].Population[Species[s].mom[j]].Fitness[1] =  network.ForwardFitnessPass(Individual, Sample2);//evaluate fitness for obj2
		TotalEval++;
    }
}

/*
	--Nondominance--
	Compare two individuals and determine which one is non-dominated

*/
void   CoEvolution:: Nondominance(int s, int ind1, int ind2)
{
    int flag1=0;
    int flag2=0;

    for( int i=0; i<num_objectives; i++){
         
        if(Species[s].Population[ind1].Fitness[i] < Species[s].Population[ind2].Fitness[i]){ //indv1 performs better on given objective
            flag1=1;
         
     
        }else if(Species[s].Population[ind1].Fitness[i] > Species[s].Population[ind2].Fitness[i]){//indv2 performs better on given objective
            flag2=1;
           
 
        }
    }
 
    if(flag1==1 && flag2==0){
        flag=1; //indv1 is non-dominated
        
    }else if(flag1== 0 && flag2==1){
        flag= 2;//individual 2 dominates
        
    }else{
        flag= 0;// both non-dominated
      
    }

}
/*
    RankPopulation
	Compare all individuals to each other and determine non-dominated individuals
*/
void    CoEvolution:: RankPopulation(int s)
{
   
    int j=0;
    for(int i=0; i<PopSize; i++){
        Species[s].Population[i].rank=0;
    }
    
    for(int i=0; i<PopSize; i++){
       
        while(j<PopSize){
             
            Nondominance(s, i,j);
             
            if(flag==1){
                Species[s].Population[j].rank+=1; //increase rank of dominated individual
         
             
            }else if(flag==2){
                Species[s].Population[i].rank+=1;
                 
            }
            flag=-1 ;
            j++;
        }
        j=i+2;
       
    }
     
         
}

/*
	--EvolveSubPopulations--
	Evolve and form new sub-populations
*/
void CoEvolution::  EvolveSubPopulations(int repetitions,double h, NeuralNetwork network,TrainingExamples Sample1, TrainingExamples Sample2 , double mutation,int depth, ofstream &out5)
{
	
	int count =0;
	int tag;
	kid = KIDS;
	Dataint nfront; //declare temp pareto front
	for(int i=0; i<NoSpecies; i++){
		nfront.push_back(vector<int>());//initialise
		
	} 
	
	for( int s =0; s < NoSpecies; s++) {
		if((rand()%100)<(h*100)){
            for (int r = 0; r < repetitions; r++){
            	if(NotConverged[s]){
					
	    		    Species[s].kids = KIDS;
					Species[s].RandomParents();//select random parents for mating

					for(int i=0;i<Species[s].kids;i++)
					{
												
						tag = 	Species[s].GenerateNewPCX(i,mutation, depth); //generate a child using PCX

						if (tag == 0) break;
					}
	 	   	        if (tag == 0) {
						NotConverged[s]=false;
	 	   	        }

					for(int i=0;i<	Species[s].kids;i++)
						EvalNewPop(i,s, network, Sample1, Sample2);//evaluate new sub-population for fitness

					find_parents(s,network, Sample1, Sample2);  // form a pool from which a solution is to be
	 	   	                           //   replaced by the created child
					Species[s].sort();          // sort the kids+parents by fitness of obj1
					rep_parents(s,network, Sample1, Sample2);   // a chosen parent is replaced by the child

					Species[s].BestIndex=0;//reset variable
	 	  	        

					int counter=0;			
					RankPopulation(s);//rank sub-pop
					out5<<"pop: "<< s <<endl<<endl<<endl;
					counter=0;
					for(int j=0; j<PopSize; j++){
						
						if(Species[s].Population[j].rank==0){//check if individual is non-dominated
							counter++;
							nfront[s].push_back(j);//add index to pareto front
							for(int x=0; x<2; x++){
							
									
								out5<<"Individual "<<j<<"  Fitness "<<x<<": "<<Species[s].Population[j].Fitness[x]<<endl;//output fitness of non-dominated individual
							}
						}
						
					}
					out5<<"Number of non-dominated individuals: "<<counter<<endl; //output no. of non-dominated individuals
					counter=0;
					
					int x=rand()%nfront[s].size(); //chose random index from set of non-dominated solutions
					Species[s].BestIndex=nfront[s][x];//update fittest		
                    Species[s].BestFit=Species[s].Population[Species[s].BestIndex].Fitness[0];
					GetBestTable(s);//update best individuals from all subpops
					Join();//concatenate the individual to be evaluated with the best individuals from other subpops 
	 	  	    }
            }
		}
	}
	pfront=nfront; //temp front assigned to permanent archive

}

/*
	--CoEvolution--
*/
class CombinedEvolution        :    public    CoEvolution 
{
    public:
		//class variables
		int TotalEval;
		int TotalSize;
		double Train;
		double TrainNMSE[num_objectives];
		double TestNMSE[num_objectives];
		double TrainRMSE[num_objectives];
		double TestRMSE[num_objectives];		
		double Test;
		double Error;
		//Layer testing_result;		
		CoEvolution NeuronLevel;
		CoEvolution WeightLevel;
		CoEvolution OneLevel;
		int Cycles;
		bool Sucess;

		CombinedEvolution(){
			//constructor
		}
		//class functions
        int GetEval(){
            return TotalEval;
        }
        
		int GetCycle(){
            return Cycles;
        }
        
		double GetError(){
            return Error;
        }

        double NMSETrain(int index){
            return TrainNMSE[index];
        }
        
		double NMSETest(int index){
            return TestNMSE[index];
        }

		double getRMSETest(int index){
            return TestRMSE[index];
        }
		double setRMSETest(double result, int index){
            TestRMSE[index]=result;
        }

		double getRMSETrain(int index){
            return TrainRMSE[index];
        }
		double setRMSETrain(double result, int index){
            TrainRMSE[index]=result;
        }

        bool GetSucess(){
            return Sucess;
        }

		void   Procedure(bool bp,   double h, ofstream &out1, ofstream &out2, ofstream &out3, ofstream &out4, ofstream &out5, double mutation,double depth );
};

/*
	--Procedure--
	The entire algorithm
*/
void    CombinedEvolution::Procedure(bool bp,   double h, ofstream &out1, ofstream &out2, ofstream &out3, ofstream &out4, ofstream &out5, double mutation,double depth )
{
	clock_t start = clock();
	int hidden =h;
	int weightsize1 = (input*hidden);//number of weights between hidden and input layer
	int weightsize2 = (hidden*output);//number of weights between hidden and output layer
	int biasize =   hidden + output; //bias for hidden and output layer

	double trainpercent=0;
	double testpercent=0;
	int epoch;
	double testtree;
	char  file1[15] = "Learnt1.txt"; //save neural network weights for obj1
	char  file2[15] = "Learnt2.txt"; //save neural network weights for obj2
	double H = 0;
	int gene = 1;
	TotalEval = 0;

	TrainingExamples Sample1(trainfile1,trainsize1 ,input+output, input, output); //data set for obj1
	TrainingExamples Sample2(trainfile2,trainsize2 ,input+output, input, output); //data set for obj2
	Sample1.printData();//print the data for obj1
	Sample2.printData();//print data for obj2
	double error;

	Sizes layersize;
	layersize.push_back(input);//set size(neurons) of input layer
	layersize.push_back(hidden);//set size(neurons) of hidden layer--only one hidden layer
	layersize.push_back(output); //set size(neurons) of output layer

    NeuralNetwork network(layersize); //initialize network
	network.CreateNetwork(layersize,Sample1 );//setup neural network

	if(bp){
		
		//gene = network.BackPropogation(  Samples,0.1,layersize, file, true); //use backpropogation
		
	}else{
		Sucess= false;
		Cycles =0;
		OneLevel.SpeciesSize.push_back(weightsize1+ weightsize2+   biasize); //set species size
		OneLevel.NoSpecies = 1; //only one subpop
		OneLevel.InitializeSpecies(CCPOPSIZE);//initialize sub-population

		#ifdef networklevel
			OneLevel.EvaluateSpecies(network, Sample1, Sample2, out5);//evaluate sub-populations
		#endif
	
		for( int s =0; s < OneLevel.NoSpecies; s++)
			OneLevel.NotConverged[s]=true;
			
		cout<<" Evaluated OneLevel ----------->"<<endl;
		for(int n = 0; n < (weightsize1+ weightsize2  + biasize) ; n++)
			WeightLevel.SpeciesSize.push_back(1);////set species size
		
		WeightLevel.NoSpecies = weightsize1+ weightsize2  + biasize; //number of sub-pops depends on weights and bias for synapse level
		WeightLevel.InitializeSpecies(CCPOPSIZE);//initialize sub-populations
		#ifdef weightlevel
			WeightLevel.EvaluateSpecies(network, Sample1, Sample2, out5);//evaluate sub-populations
		#endif
		for( int s =0; s < WeightLevel.NoSpecies; s++)
			WeightLevel.NotConverged[s]=true;

		cout<<" Evaluated WeightLevel ----------->"<<endl;

		for(int n = 0; n < hidden ; n++)
			NeuronLevel.SpeciesSize.push_back(input+1); //set species size (hidden layer)

		for(int n = 0; n < output ; n++)
			NeuronLevel.SpeciesSize.push_back(hidden+1);//set species size in output layer

		NeuronLevel.NoSpecies =  hidden+output; //number of subpops is equal to number of hidden + ouput neurons
		NeuronLevel.InitializeSpecies(CCPOPSIZE);//initialise sub-populations
		#ifdef neuronlevel
			NeuronLevel.EvaluateSpecies(network,Sample1, Sample2, out5);//evaluate sub-populations
		#endif 
		cout<<h<<' '<<NeuronLevel.NoSpecies<<endl;
		for( int s =0; s < NeuronLevel.NoSpecies; s++)
			NeuronLevel.NotConverged[s]=true;

		cout<<" Evaluated neuronLevel ----------->"<<endl;

		TotalEval=0;
		NeuronLevel.TotalEval=0;
		OneLevel.TotalEval=0;
		WeightLevel.TotalEval=0;
		int count =0;
		Layer ErrorArray;

		#ifdef networklevel
			while(TotalEval<=maxgen){ // while the maximum number of function evaluations is not reached
				OneLevel.EvolveSubPopulations(1, 1 ,network, Samples,mutation,0);//evolve sub-population
				OneLevel.Join();//concatenate best individuals to form complete solution
				network.ChoromesToNeurons(OneLevel.Individual);//encode into neural network
				network.SaveLearnedData(layersize, file); //save neural network weights
				Error =OneLevel.Species[OneLevel.NoSpecies-1].Population[OneLevel.Species[OneLevel.NoSpecies-1].BestIndex].Fitness; 
				out1<<hidden<<" wl "<<Train<<"    "<<Error<<"    "<< OneLevel.TotalEval<<"    "<<count<<endl; //shows how RMSE is going down 
				ErrorArray.push_back(Error); //store error
				TotalEval=OneLevel.TotalEval; 
				
				if(Error < MinimumError){ 
					Sucess = true;
					break;
				} 
				count++;      	      	                  	      
			}  
		#endif	 

		#ifdef neuronlevel
			while(TotalEval<=maxgen){ //while the maximum number of function evaluations is not reached
				NeuronLevel.EvolveSubPopulations(1, 1 ,network, Sample1, Sample2, mutation,0, out5);//evolve sub-populations in round-robin fashion
				NeuronLevel.GetBestTable(NeuronLevel.NoSpecies-1); //get best individuals from all subpops
				NeuronLevel.Join();//concatenate best individuals to form complete solution
				network.ChoromesToNeurons(NeuronLevel.Individual);//encode into neural network
				for(int h=0; h<num_objectives; h++){
					if(h==0){
						network.SaveLearnedData(layersize, file1);//save the neural network weights for obj1
					}else{	
						network.SaveLearnedData(layersize, file2); //save the neural network weights for obj1
					}
				 
				} 
				
				ErrorArray.push_back(Error);
				TotalEval=NeuronLevel.TotalEval; 
				count++;      	      	                  	      
			}  
		#endif    

		#ifdef weightlevel
			while(TotalEval<=maxgen){//while the maximum number of function evaluations is not reached
				WeightLevel.EvolveSubPopulations(1, 1 ,network, Samples,mutation,0);//evolve sub-populations in round robin fashion
				WeightLevel.Join();//concatenate best individuals to form complete solution
				network.ChoromesToNeurons(WeightLevel.Individual);//encode into neural networks
				network.SaveLearnedData(layersize, file);//save the neural networks weights
				Error =WeightLevel.Species[WeightLevel.NoSpecies-1].Population[WeightLevel.Species[WeightLevel.NoSpecies-1].BestIndex].Fitness; 
				
				if(count%10==0)
					out1<<hidden<<" wl "<<Train<<"    "<<Error<<"    "<< WeightLevel.TotalEval<<"    "<<count<<endl;//shows how the RMSE is going down with 
																													//each function evaluation
				ErrorArray.push_back(Error);//store error for this evaluation
				TotalEval=WeightLevel.TotalEval; 

				if(Error < MinimumError){ 
					Sucess = true;
					break;
				} 
				count++;	      	      	                  	      
			}  
	  
		#endif        
		
		
		
		//objective 1---------------------------------------------------------------------------------

		out1<<"Train"<<endl; 
		//test the neural network with training data
		Train =  network.TestTrainingData(layersize,file1, trainsize1,trainfile1,input,output, out1, out2, 1); 
		setRMSETrain(Train, 0);
		TrainNMSE[0] =       network.NMSError();

		out1<<"Test"<<endl;
		//test the neural network and the learnt data with the testing data set
		Test =  network.TestTrainingData(layersize,file1,testsize1,testfile1, input,output, out1, out2, 1); 
		setRMSETest(Test, 0);
		TestNMSE[0] =       network.NMSError();

		out1<<endl; 
		out1<<" ------------------------------ "<<h<<"  "<<TotalEval<<"  "<<Train<<"  "<<Test<<endl;
		out1<<endl;
		
		out3<<"  "<<h<<"  "<<TotalEval<<"  RMSE:  "<<Train<<"  "<<Test<<" NMSE:  "<<TrainNMSE[0]<<" "<<TestNMSE[0]<<endl;
		
		//-------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------
		//objective 2
		out2<<"Train"<<endl; 
		//test the neural network with training data
		Train =  network.TestTrainingData(layersize,file2, trainsize2,trainfile2,input,output,out1, out2, 2); 
		setRMSETrain(Train, 1);
		TrainNMSE[1] =       network.NMSError();

		out2<<"Test"<<endl;
		//test the neural network and the learnt data with the testing data set
		Test =  network.TestTrainingData(layersize,file2,testsize2,testfile2, input,output,out1, out2, 2); 
		setRMSETest(Test, 1);
		TestNMSE[1] =       network.NMSError();

		out2<<endl; 
		out2<<" ------------------------------ "<<h<<"  "<<TotalEval<<"  "<<Train<<"  "<<Test<<endl;
		
		out2<<endl;
		out4<<"  "<<h<<"  "<<TotalEval<<"  RMSE:  "<<Train<<"  "<<Test<<" NMSE:  "<<TrainNMSE[1]<<" "<<TestNMSE[1]<<endl;
		out5<<"----------------------------------------------------------------NExt neuron--------------------------"<<endl;

		//----------------------------------------------------------------------------------------------------------------
	}
}

int main(void)
{
	int VSize =90;
	//create output files
	ofstream out1;
	out1.open("Oneout1.txt");//actual vs predicted values for obj1
	ofstream out2;
	out2.open("Oneout2.txt");//actual vs predicted values for obj2
	ofstream out3;
	out3.open("Oneout3.txt");//summary of each experimental run for obj1
    ofstream out4;
	out4.open("Oneout4.txt");//summary of each experimental run for obj2
	ofstream out5;
	out5.open("Oneout5.txt");//tracks all non-dominated individuals for each evaluation
	ofstream out8;
	out8.open("Oneout8.txt");//summary of each hidden neuron after x number of experimental runs for obj1
	ofstream out9;
	out9.open("Oneout9.txt");//summary of each hidden neuron after x number of experimental runs for obj2

    for(double hidden=3;hidden<=9;hidden+=2){
	for(double onelevelstop=0.01;onelevelstop>=0.01;onelevelstop-=0.002){
		//variable declaration
		Sizes EvalAverage;
		Layer ErrorAverage;//error aeverage
		Layer CycleAverage;
		Layer NMSETrainAve_0;
		Layer NMSETrainAve_1;//store NMSE training average
		Layer NMSETestAve_0;
		Layer NMSETestAve_1;//store NMSE test average
		Layer RMSETest_0;
		Layer RMSETest_1;
		Layer RMSETrain_0;
		Layer RMSETrain_1;
		int MeanEval=0;
		double MeanError=0;//initialize all to zeros
		double MeanCycle=0;
		double NMSETrainMean_0=0;
		double NMSETrainMean_1=0;
		double NMSETestMean_0=0;
		double NMSETestMean_1=0;
		int EvalSum=0;
		double NMSETrainSum_0 = 0;
		double NMSETrainSum_1 = 0;
		double NMSETestSum_0 = 0;
		double NMSETestSum_1 = 0;
		double ErrorSum_0=0;
		double ErrorSum_1=0;
		double TrainErrorSum_0=0;
		double TrainErrorSum_1=0;
		double CycleSum=0;
		double maxrun =50;//number of experimental runs
		int success = 0;
		double test_average_0=0;
		double test_average_1=0;
		double test_best_rmse_0=0;
		double test_best_rmse_1=0;
		double test_sum_0=0;
		double test_sum_1=0;
		double test_best_nmse_0=0;
		double test_best_nmse_1=0;
		double train_average_0=0;
		double train_average_1=0;
		double train_best_rmse_0=0;
		double train_best_rmse_1=0;
		double train_sum_0=0;
		double train_sum_1=0;
		

		for(int run=1;run<=maxrun;run++){
			CombinedEvolution Combined;

			Combined.Procedure(false, hidden, out1, out2, out3, out4, out5, 0  , onelevelstop );//run the main CC algorithm

			if(Combined.GetSucess()){
				success++;
			}
			
			//OBJ1				
			NMSETrainAve_0.push_back(Combined.NMSETrain(0));
			NMSETrainMean_0+=Combined.NMSETrain(0);
			//OBJ2
			NMSETrainAve_1.push_back(Combined.NMSETrain(1));
			NMSETrainMean_1+=Combined.NMSETrain(1);

			//obj1
			NMSETestAve_0.push_back(Combined.NMSETest(0));
			NMSETestMean_0+=Combined.NMSETest(0);
			//obj2
			NMSETestAve_1.push_back(Combined.NMSETest(1));
			NMSETestMean_1+=Combined.NMSETest(1);
			
			//obj1
			RMSETrain_0.push_back(Combined.getRMSETrain(0));
			RMSETest_0.push_back(Combined.getRMSETest(0));
			//obj2
			RMSETrain_1.push_back(Combined.getRMSETrain(1));
			RMSETest_1.push_back(Combined.getRMSETest(1));
			
		}//run

		NMSETestMean_0/= NMSETestAve_0.size();
		NMSETrainMean_0 /= NMSETrainAve_0.size();
		NMSETestMean_1/= NMSETestAve_1.size();
		NMSETrainMean_1 /= NMSETrainAve_1.size();

		//calculating average test rmse	-- obj1		
		for(int y=0; y<RMSETest_0.size(); y++){
			test_sum_0=test_sum_0 + RMSETest_0[y];
		}
		test_average_0=test_sum_0/RMSETest_0.size();

		//calculating average test rmse	-- obj2		
		for(int y=0; y<RMSETest_1.size(); y++){
			test_sum_1=test_sum_1 + RMSETest_1[y];
		}
		test_average_1=test_sum_1/RMSETest_1.size();
		
		//calculating best test rmse--obj1
		test_best_rmse_0=RMSETest_0[0];
		for(int i=1; i<RMSETest_0.size(); i++){
			if(test_best_rmse_0 > RMSETest_0[i]){
				test_best_rmse_0=RMSETest_0[i];
			}
		}
		
		//calculating best test rmse--obj2
		test_best_rmse_1=RMSETest_1[0];
		for(int i=1; i<RMSETest_1.size(); i++){
			if(test_best_rmse_1 > RMSETest_1[i]){
				test_best_rmse_1=RMSETest_1[i];
			}
		}


		//calculating average train rmse--obj1			
		for(int n=0; n<RMSETrain_0.size(); n++){
			train_sum_0=train_sum_0 + RMSETrain_0[n];
		}
		train_average_0=train_sum_0/RMSETrain_0.size();

		//calculating average train rmse--obj2			
		for(int n=0; n<RMSETrain_1.size(); n++){
			train_sum_1=train_sum_1 + RMSETrain_1[n];
		}
		train_average_1=train_sum_1/RMSETrain_1.size();
		
		//calculating best train rmse--obj1
		train_best_rmse_0=RMSETrain_0[0];
		for(int x=1; x<RMSETrain_0.size(); x++){
			if(train_best_rmse_0 > RMSETrain_0[x]){
				train_best_rmse_0=RMSETrain_0[x];
			}
		}

		//calculating best train rmse---obj2
		train_best_rmse_1=RMSETrain_1[0];
		for(int x=1; x<RMSETrain_1.size(); x++){
			if(train_best_rmse_1 > RMSETrain_1[x]){
				train_best_rmse_1=RMSETrain_1[x];
			}
		}

		//calculating best test nmse--obj1
		test_best_nmse_0=NMSETestAve_0[0];
		for(int j=1; j<NMSETestAve_0.size(); j++){
			if(test_best_nmse_0 > NMSETestAve_0[j]){
				test_best_nmse_0=NMSETestAve_0[j];
			}
		}

		
		//calculating best test nmse--obj2
		test_best_nmse_1=NMSETestAve_1[0];
		for(int j=1; j<NMSETestAve_1.size(); j++){
			if(test_best_nmse_1 > NMSETestAve_1[j]){
				test_best_nmse_1=NMSETestAve_1[j];
			}
		}


		//CI for training nmse--obj1
		for(int a=0; a < NMSETrainAve_0.size();a++)
			NMSETrainSum_0 +=	(NMSETrainAve_0[a]-NMSETrainMean_0)*(NMSETrainAve_0[a]-NMSETrainMean_0);
		
		NMSETrainSum_0/= NMSETrainAve_0.size();
		NMSETrainSum_0 = sqrt(NMSETrainSum_0);
		NMSETrainSum_0 = 1.96 * (NMSETrainSum_0/sqrt(NMSETrainAve_0.size()));

		//CI for training nmse--obj2
		for(int a=0; a < NMSETrainAve_1.size();a++)
			NMSETrainSum_1 +=	(NMSETrainAve_1[a]-NMSETrainMean_1)*(NMSETrainAve_1[a]-NMSETrainMean_1);
		
		NMSETrainSum_1/= NMSETrainAve_1.size();
		NMSETrainSum_1 = sqrt(NMSETrainSum_1);
		NMSETrainSum_1 = 1.96 * (NMSETrainSum_1/sqrt(NMSETrainAve_1.size()));
		
		//CI for test nmse--obj1
		for(int a=0; a < NMSETestAve_0.size();a++)
			NMSETestSum_0 +=	(NMSETestAve_0[a]-NMSETestMean_0)*(NMSETestAve_0[a]-NMSETestMean_0);
		
		NMSETestSum_0/= NMSETestAve_0.size();
		NMSETestSum_0 = sqrt(NMSETestSum_0);
		NMSETestSum_0 = 1.96 * (NMSETestSum_0/sqrt(NMSETestAve_0.size()));

		//CI for test nmse--obj2
		for(int a=0; a < NMSETestAve_1.size();a++)
			NMSETestSum_1 +=	(NMSETestAve_1[a]-NMSETestMean_1)*(NMSETestAve_1[a]-NMSETestMean_1);
		
		NMSETestSum_1/= NMSETestAve_1.size();
		NMSETestSum_1 = sqrt(NMSETestSum_1);
		NMSETestSum_1 = 1.96 * (NMSETestSum_1/sqrt(NMSETestAve_1.size()));
	 
			//CI for test rmse--obj1
		for(int a=0; a < RMSETest_0.size();a++)
			ErrorSum_0 +=	(RMSETest_0[a]-test_average_0)*(RMSETest_0[a]-test_average_0);

		ErrorSum_0=ErrorSum_0/ RMSETest_0.size();
		ErrorSum_0 = sqrt(ErrorSum_0);
		ErrorSum_0 = 1.96*(ErrorSum_0/sqrt(RMSETest_0.size()) );

		//CI for test rmse--obj2
		for(int a=0; a < RMSETest_1.size();a++)
			ErrorSum_1 +=	(RMSETest_1[a]-test_average_1)*(RMSETest_1[a]-test_average_1);

		ErrorSum_1=ErrorSum_1/ RMSETest_1.size();
		ErrorSum_1 = sqrt(ErrorSum_1);
		ErrorSum_1 = 1.96*(ErrorSum_1/sqrt(RMSETest_1.size()) );  
		
		//CI for train rmse--obj1
		for(int b=0; b < RMSETrain_0.size();b++)
			TrainErrorSum_0 +=	(RMSETrain_0[b]-train_average_0)*(RMSETrain_0[b]-train_average_0);

		TrainErrorSum_0=TrainErrorSum_0/ RMSETrain_0.size();
		TrainErrorSum_0 = sqrt(TrainErrorSum_0);
		TrainErrorSum_0 = 1.96*(TrainErrorSum_0/sqrt(RMSETrain_0.size()) ); 

		//CI for train rmse--obj2
		for(int b=0; b < RMSETrain_1.size();b++)
			TrainErrorSum_1 +=	(RMSETrain_1[b]-train_average_1)*(RMSETrain_1[b]-train_average_1);

		TrainErrorSum_1=TrainErrorSum_1/ RMSETrain_1.size();
		TrainErrorSum_1 = sqrt(TrainErrorSum_1);
		TrainErrorSum_1 = 1.96*(TrainErrorSum_1/sqrt(RMSETrain_1.size()) ); 
		
		//final summary for obj1
		out8<< " "<<hidden<<"     "<<MeanEval<<"     "<<train_average_0<<" "<<TrainErrorSum_0<<"      "<<test_average_0<<" "<<ErrorSum_0<<"      "<<test_best_rmse_0<< "  NMSE      "  << NMSETrainMean_0   <<" "<< NMSETrainSum_0   <<"        "<< NMSETestMean_0  <<" "<<NMSETestSum_0  <<"      "<<test_best_nmse_0<<endl; 
		//final summary for obj2
		out9<< " "<<hidden<<"     "<<MeanEval<<"     "<<train_average_1<<" "<<TrainErrorSum_1<<"      "<<test_average_1<<" "<<ErrorSum_1<<"      "<<test_best_rmse_1<< "  NMSE      "  << NMSETrainMean_1   <<" "<< NMSETrainSum_1   <<"        "<< NMSETestMean_1  <<" "<<NMSETestSum_1  <<"      "<<test_best_nmse_1<<endl; 

		EvalAverage.empty();
		ErrorAverage.empty();
		CycleAverage.empty();
	}
    }//hidden
	
	out3<<"\\hline"<<endl;
	
	//close output stream
	out1.close();
	out2.close();
	out3.close();
    out4.close();
	out5.close();
	out8.close();
	out9.close();

	return 0;
};
